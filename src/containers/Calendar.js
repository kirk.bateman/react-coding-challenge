import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as reminderActions from '../actions/actions';
import React from "react";
import MonthHeader from "../components/MonthHeader";
import WeekDaysHeader from "../components/WeekDaysHeader";
import Day from "../components/Day";
import Reminder from "../components/Reminder";
import Modal from "../components/Modal";
import ReminderForm from "../components/ReminderForm";
import dateFns from "date-fns";
import moment from 'moment';

const defaultColour = '#ff0000';

class Calendar extends React.Component {

  state = {
    currentMonth : new Date(),
    selectedDate : new Date(),
    showRemindersModal: false,
    editReminder: {
      id: null,
      date: new Date(),
      description: '',
      colour: defaultColour
    }
  };

  hideModal = () => {
    // close the reminder form modal and reset editReminder
    this.setState({ showRemindersModal: false, editReminder: {
      id: null,
      date: this.state.selectedDate,
      description: '',
      colour: defaultColour
    } });
  };

  // selected a date in the month, show the modal with the
  // list of reminders and the form
  onDateClick = day => {
    this.setState({showRemindersModal: true, selectedDate : day});
  }

  // ACTION - update an existing reminder (callback from form)
  editReminder = reminder => {
    console.log(`Edit Reminder:`, reminder);

    // time comes from the rc-time-picker value
    let time = moment(reminder.time, 'HH:mm');
    reminder.date.setHours(time.hours())
    reminder.date.setMinutes(time.minutes());

    // dispatch update action
    this.props.reminderActions.updateReminder(reminder.id, reminder.description, reminder.colour, reminder.date);
  }

  // ACTION - add a new reminder (callback from form)
  addReminder = reminder => {
    console.log(`Adding new Reminder`, reminder);

    // time comes from the rc-time-picker value
    let time = moment(reminder.time, 'HH:mm');
    reminder.date.setHours(time.hours())
    reminder.date.setMinutes(time.minutes());

    // dispatch update action
    this.props.reminderActions.addReminder(reminder.description, reminder.colour, reminder.date);
  }

  // ACTION - delete selected reminder
  deleteReminder = reminderId => {
    this.props.reminderActions.delReminder(reminderId);
  }

  // update the colour in the current reminder
  handleSetColour = data => {
    this.setState({
      editReminder: {
        ...this.state.editReminder,
        colour: data.color
      }
    });
  };

  // setup the currently editting reminder (for the form)
  handleSetEdit = reminder => {
    // force date if we have no existing reminder
    if (reminder !== null) {
      this.handleSetEditDay(reminder.date);
    } else {
      this.handleSetEditDay(this.state.selectedDate);
    }

    this.setState({
      editReminder: {
        ...this.state.editReminder,
        ...reminder
      }
    });
  };

  // set the currently selected day (used to create reminders)
  handleSetEditDay = day => {
    this.setState({
      selectedDate: day
    });
  };

  handleCreateOrUpdateReminder = (e, update) => {
    e.preventDefault();

    const form = e.target;
    const description = form.querySelector(".description").value.trim();

    if (description.length) {
      const payload = {
        date: this.state.selectedDate,
        time: form.querySelector(".rc-time-picker-input").value,
        description: description,
        colour: this.state.editReminder.colour || defaultColour
      };

      if (update.id) {
        payload["id"] = update.id;
        this.editReminder(payload);
      } else {
        this.addReminder(payload);
      }

      this.setState({ editReminder: {} });
    }

  }

  /**
  * [getRemindersByDay get array of reminders for specific day (sorted by time)]
  * @param  {[Date]} currentMonthDate [date to get reminders for (ignores time)]
  * @return {[Array]}                  [array of reminders]
  */
  getRemindersByDay(currentMonthDate) {
    let thisDayReminders = this.props.reminders.filter(reminder => dateFns.isSameDay(reminder.date, currentMonthDate));

    // sort by date just incase
    thisDayReminders.sort(function compare(a, b) {
      var dateA = new Date(a.date);
      var dateB = new Date(b.date);
      return dateA - dateB;
    });

    return thisDayReminders
  }


  renderDays() {
    const { currentMonth, selectedDate } = this.state;
    const monthStart = dateFns.startOfMonth(currentMonth);
    const monthEnd = dateFns.endOfMonth(monthStart);
    const startDate = dateFns.startOfWeek(monthStart);
    const endDate = dateFns.endOfWeek(monthEnd);

    const dateFormat = "D";
    let rows = [];
    let days = [];
    let day = startDate;
    let formattedDate = "";
    // starting at the start of the week (sunday) for this month
    // that can be before the 1st of the month (they should be disabled)
    while (day <= endDate) {
      for (let i = 0; i < 7; i++) {
        formattedDate = dateFns.format(day, dateFormat);

        // clone the current day as we are going to modify it while iterating
        const cDay = day;
        // if the day is not in this actual month disable it
        if (!dateFns.isSameMonth(day, monthStart)) {
          days.push(
            <Day className={`col cell disabled`} key={day} />
          )
        } else {
          // a Day cell basically consists of the cell itself
          // any reminders that are on this day and the number of the day
          const reminders = this.getRemindersByDay(cDay);
          days.push(
            <Day
            className={`col cell ${dateFns.isSameDay(day, selectedDate) ? "selected" : ""}`}
            key={day}
            onClick={() => this.onDateClick(dateFns.parse(cDay))}
            >
            {reminders.map((reminder, idx) => <Reminder key={idx} reminder={reminder}/>)}

            <span className="number">{formattedDate}</span>
            </Day>
          );

        }
        day = dateFns.addDays(day, 1);
      }
      // push this week of days into a row
      rows.push(
        <div className="row" key={day}>
        {days}
        </div>
      );
      days = [];
    }

    // return the array of rows (weeks)
    return <div className="body">{rows}</div>;
  }


  renderDayModal(reminders) {
    return (
      <Modal show={this.state.showRemindersModal} handleClose={this.hideModal}>
      <p>Reminders for {dateFns.format(this.state.selectedDate, 'ddd Do MMM')}</p>
      { (reminders.length > 0) ? (
        <table>
        <tbody>
        {reminders.map((reminder, idx) =>
          <tr key={idx} style={{ backgroundColor: reminder.colour}}>
          <td>{reminder.description}</td>
          <td> at {dateFns.format(reminder.date, 'h:mma')} - </td>
          <td><div title="Edit" className="icon action" onClick={() => this.handleSetEdit(reminder)}><i className="fa fa-pencil"></i> Edit</div></td>
          <td><div title="Delete" className="icon action" onClick={() => this.deleteReminder(reminder.id)}><i className="fa fa-trash"></i> Delete</div></td>
          </tr>)}
          </tbody>
          </table>
        ) : 'There are no reminders today' }
        <h3>
        {this.state.editReminder.id ? 'Edit Reminder Below' : 'Add New Reminder Below'}
        </h3>
        <ReminderForm
        reminder={this.state.editReminder}
        handleSetColour={this.handleSetColour}
        handleSetEditDay={this.handleSetEditDay}
        handleCreateOrUpdateReminder={this.handleCreateOrUpdateReminder}
        defaultColour={defaultColour}
        />
        </Modal>
      );

    };



    render() {
      // used by current modal list view
      const selectedDayReminders = this.getRemindersByDay(this.state.selectedDate);

      return (
        <div className={this.props.className}>
        {this.renderDayModal(selectedDayReminders)}
        <MonthHeader
          onPrevMonthClick={() => {this.setState({currentMonth: dateFns.subMonths(this.state.currentMonth, 1)})}}
          onNextMonthClick={() => {this.setState({currentMonth: dateFns.addMonths(this.state.currentMonth, 1)})}}
          currentMonth={this.state.currentMonth}
          dateFormat={'MMMM YYYY'}/>
        <WeekDaysHeader currentMonth={this.state.currentMonth} />
        {this.renderDays()}
        </div>
      );
    }
  }

  function mapStateToProps(state) {
    return { reminders: state.reminders };
  }

  function mapDispatchToProps(dispatch) {
    return {
      reminderActions: bindActionCreators(reminderActions, dispatch)
    };
  }

  export default connect(mapStateToProps, mapDispatchToProps)(Calendar);
