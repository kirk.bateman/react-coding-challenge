import initialState from './initialState';
import {ADD_REMINDER, DEL_REMINDER, UPDATE_REMINDER} from '../actions/actionTypes';
import uuid from 'uuid/v1';


export default function reminders(state = initialState.reminders, action) {
  let newState;

  switch (action.type) {
    case ADD_REMINDER:
      newState = [
              ...state,
              {
                id: uuid(),
                description: action.payload.description,
                colour: action.payload.colour,
                date: action.payload.date
              }
            ];
      return newState;

    case DEL_REMINDER:
      newState = state.filter(reminder => reminder.id !== action.payload.id);
      return newState;

    case UPDATE_REMINDER:
      newState = state.map(
        reminder =>
          reminder.id === action.payload.id ? { ...reminder, description: action.payload.description, colour: action.payload.colour, date: action.payload.date } : reminder
      )
      return newState;
    default:
      return state;
  }
}
