import {combineReducers} from 'redux';
import reminders from './remindersReducer';

const rootReducer = combineReducers({
  reminders
});

export default rootReducer;
