export default {
  reminders: [{id: '12342', description: 'One', colour: '#ffeeaa', date: new Date('2019-01-08T18:00:00Z')},
  {id: '12343', description: 'Two', colour: '#ffdddd', date: new Date('2019-01-05T18:00:00Z')},
  {id: '12344', description: 'Three', colour: '#ffccee', date: new Date('2019-01-05T13:00:00Z')},
	{id: '12345', description: 'Four', colour: '#ffaaaa', date: new Date('2019-01-05T17:00:00Z')}
  ]
};
