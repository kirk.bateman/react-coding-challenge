import React from "react";
import dateFns from 'date-fns';


export default class WeekDaysHeader extends React.Component {

render() {

				const dateFormat = "dddd";
				const days = [];
				let startDate = dateFns.startOfWeek(this.props.currentMonth);
				for (let i = 0; i < 7; i++) {
					days.push(
					<div className="col col-centre" key={i}>
					{dateFns.format(dateFns.addDays(startDate, i), dateFormat)}
					</div>
					);
				}
				return <div className="days row">{days}</div>;
			};

};
