import React, { Component } from 'react';

// rendered within a day component
// rendered a maximum of 25 chars
class Reminder extends Component {

render() {
	return (
		<>
		<span
			key={this.props.reminder.id}
			className="remindercell"
			style={{ borderLeft: '10px solid', color: this.props.reminder.colour}}>
			{this.props.reminder.description.substring(0, 25)}
		</span>
		</>
		);
}
}

export default Reminder;
