import React from "react";
import dateFns from 'date-fns';

const monthHeader = props => (
  <header className="header row">
    <div className="col col-start action" style={{fontSize: '4em'}} onClick={props.onPrevMonthClick}>
      <i className="fa fa-arrow-left"></i>
    </div>
    <div className="col col-centre">
      <h1>{dateFns.format(props.currentMonth, props.dateFormat)}</h1>
    </div>
    <div className="col col-end action" style={{fontSize: '4em'}} onClick={props.onNextMonthClick}>
      <i className="fa fa-arrow-right"></i>
    </div>
  </header>
);

export default monthHeader;
