import React from "react";
import TimePicker from "rc-time-picker";
import ColorPicker from "rc-color-picker";
import moment from 'moment';
import "rc-time-picker/assets/index.css";
import "rc-color-picker/assets/index.css";

class reminderForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      reminder: {
        id: props.reminder.id,
        description: props.reminder.description,
        date: props.reminder.date,
        time: moment(props.reminder.date),
        colour: props.reminder.colour ? props.reminder.colour : props.defaultColour
      }
    }

    this.handleDescriptionChanged = this.handleDescriptionChanged.bind(this);
    this.handleTimeChanged = this.handleTimeChanged.bind(this);
    this.handleColourChanged = this.handleColourChanged.bind(this);
    this.resetForm = this.resetForm.bind(this);
  }


  // allow for updating details when selecting a different reminder or starting a new one
  static getDerivedStateFromProps(props, current_state) {
    if ((current_state.reminder.id !== props.reminder.id)) {
      return {
        reminder: {
          id: props.reminder.id,
          description: props.reminder.description,
          date: props.reminder.date,
          time: moment(props.reminder.date),
          colour: props.reminder.colour ? props.reminder.colour : props.defaultColour
        }
      }

    }
    return null
  }

  resetForm = (e) => {
    e.preventDefault();

    let newState = {
      reminder: {
        id: null,
        description: '',
        date: new Date(),
        time: moment(),
        colour: this.props.defaultColour
      }
    };

    this.setState(newState);
  }

  handleDescriptionChanged = (e) => {
    let newState = {reminder: {...this.state.reminder, description: e.target.value}};
    this.setState(newState);
  }

  handleTimeChanged = (newTime) => {
    let newState = {reminder: {...this.state.reminder, time: newTime}};
    this.setState(newState);
  }

  handleColourChanged = (e) => {
    let newState = {reminder: {...this.state.reminder, colour: e.color}};
    this.setState(newState);

    // callback
    this.props.handleSetColour(e);
  }

  render() {
    const time = this.state.reminder.time
    ? moment(this.state.reminder.time, "HH:mm")
    : moment()
    .hour(0)
    .minute(0);

    return (
      <form className="row col col-centre"
      method="post"
      onSubmit={e => this.props.handleCreateOrUpdateReminder(e, this.state.reminder)}
      >
      <textarea
      className="description"
      placeholder="Reminder"
      maxLength="25"
      value={this.state.reminder.description ? this.state.reminder.description : ''}
      onChange={this.handleDescriptionChanged}
      />

      <TimePicker
      showSecond={false}
      format="HH:mm"
      value={time}
      use24Hours
      inputReadOnly
      onChange={this.handleTimeChanged}
      />

      <ColorPicker
      className="color-picker"
      color={this.state.reminder.colour || this.props.defaultColour}
      onClose={this.handleColourChanged}
      />

      <button className="btn-submit">{ this.state.reminder.id ? 'Update Reminder' : 'Add Reminder' }</button>

      <button
      className="btn-cancel"
      onClick={ this.resetForm }
      >
      Reset
      </button>
      </form>
    );
  }

};

export default reminderForm;
