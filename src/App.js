import React, { Component } from 'react';
import Calendar from './containers/Calendar';
import './App.css';


class App extends Component {
  render() {
    return (
      <div className="app">
        <Calendar className="calendar"/>
      </div>
    );
  }
}

export default App;
