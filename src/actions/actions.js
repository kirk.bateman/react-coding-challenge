import * as types from './actionTypes';


// action creators

export function addReminder(desc, colour, date) {
  return {type: types.ADD_REMINDER, payload: {description: desc, colour: colour, date: date} };
}

export function delReminder(reminderId) {
  return {type: types.DEL_REMINDER, payload: {id: reminderId} };
}

export function updateReminder(reminderId, newDesc, newColour, newDate) {
  return {type: types.UPDATE_REMINDER, payload: {id: reminderId, description: newDesc, colour: newColour, date: newDate} };
}
